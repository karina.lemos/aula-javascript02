function maiormediadecadaturma (objeto){
    //Variáveis para guardar os nomes dos alunos com as maiores médias de cada turma!
    var nome1 = "a";
    var nome2 = "b";
    var nome3 = "c";
   
    for (i in objeto){
        //Aqui vai analisar só a turma A
        if (objeto[i]['turma'] == 'A'){ 
            var maiormediaturmaA = 0;
            var media = (objeto[i]['nota1'] + objeto[i]['nota2'])/2; 
            
            if (media> maiormediaturmaA){ 
                nome1= objeto[i]['nome']; //Guarda o nome do aluno q possui a maior média
                maiormediaturmaA = media; //Guarda a maior média da turma A
            }

        }

        //Aqui vai analisar só a turma B
        if (objeto[i]['turma'] == 'B'){ 
            var maiormediaturmaB = 0;
            var media2 = (objeto[i]['nota1'] + objeto[i]['nota2'])/2;
            
            if (media2> maiormediaturmaB){ 
                nome2= objeto[i]['nome']; //Guarda o nome do aluno q possui a maior média
                maiormediaturmaB = media2; //Guarda a maior média da turma A
            }

        }

        //Aqui vai analisar só a turma C
        if (objeto[i]['turma'] == 'C'){ 
            var maiormediaturmaC = 0;
            var media3 = (objeto[i]['nota1'] + objeto[i]['nota2'])/2; 
            
            if (media3> maiormediaturmaC){ 
                nome3= objeto[i]['nome']; //Guarda o nome do aluno q possui a maior média
                maiormediaturmaC = media3; //Guarda a maior média da turma A
            }

        }
    
    }

    console.log("O aluno ", nome1, "teve a média mais alta da turma A  com " + maiormediaturmaA);
    console.log("O aluno ", nome2, "teve a média mais alta da turma B  com " + maiormediaturmaB);
    console.log("O aluno ", nome3, "teve a média mais alta da turma C  com " + maiormediaturmaC);

}

//Exemplo para teste
let escola= [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Caio",
        "turma": "A",
        "nota1": 8,
        "nota2": 7
    },
    {
        "nome": "Babi",
        "turma": "B",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Ana",
        "turma": "C",
        "nota1": 5,
        "nota2": 7
    },

]

//Para escrever na tela 
console.log(maiormediadecadaturma(escola));